const fs = require('fs')
const userName = require('os').userInfo().username
const drive = require('path').resolve(__dirname).split('\\')[0]
const outputFilePath = `${drive}/Users/${userName}/AppData/LocalLow/Wizards Of The Coast/MTGA/output_log.txt`
const prodUriPath = `${drive}/Users/${userName}/AppData/LocalLow/Wizards Of The Coast/MTGA/prodUri.txt`

let inMatch = false
let matchId, eventId
let opponent, opponentRank, opponentAvatar
let yourScreenName 
let matchCreatedJson

module.exports = {
  getInMatch: function getInMatch() {
    return inMatch
  },
  getOpponent: function getOpponent() {
    return opponent
  },
  getYourScreenName: function getYourScreenName() {
    return yourScreenName
  },
  getOpponentRank: function getOpponentRank() {
    return opponentRank
  },
  getOpponentAvatar: function getOpponentAvatar() {
    return opponentAvatar
  },
  getEventId: function getEventId() {
    return eventId
  },
  getMatchId: function getMatchId() {
    return matchId
  },
  startOutputFileWatch: function startOutputFileWatch() {
    // This will run whenever the output_file is changed
    fs.watchFile(outputFilePath, (current, previous) => {
      matchCreatedJson = getMatchCreatedJson()
      inMatch = checkForInMatch()

      if (!yourScreenName) {
        yourScreenName = getYourScreenName()
      }

      if (matchCreatedJson && inMatch) {
        opponent = getOpponentScreenName(matchCreatedJson)
        matchId = matchCreatedJson.matchId
        opponentRank = getOpponentRank( matchCreatedJson )
        opponentAvatar = matchCreatedJson.opponentAvatarSelection
        eventId = matchCreatedJson.eventId
      }
    })
  }
}

function checkForInMatch() {
  matchInfo = matchCreatedJson

  if (matchInfo) {
    let localMatchId = matchInfo.matchId
    let gameRoomState = getMatchGameRoomStateChangedEventJson()
    let gameRoomStateMatchId

    if (gameRoomState) {
      if (gameRoomState.matchGameRoomStateChangedEvent.gameRoomInfo) {
        gameRoomStateMatchId = gameRoomState.matchGameRoomStateChangedEvent.gameRoomInfo.gameRoomConfig.matchId
      }
    }

    let finalResultsJson = gameRoomState.matchGameRoomStateChangedEvent.gameRoomInfo.finalMatchResult

    if (localMatchId === gameRoomStateMatchId && finalResultsJson != undefined) {
      return false
    }

    return true
  }
  else {
    return false
  }
}

function getOpponentScreenName( json ) {
  let screenName = json.opponentScreenName

  return screenName.split('#')[0]
}
function getYourScreenName( screeName ) {
  let authRespJson = getAuthenticateResponseJson()
  let screenName = authRespJson.screenName

  return screenName.split('#')[0]
}

function getOpponentRank( json ) {
  let rankingClass = json.opponentRankingClass
  let tier = json.opponentRankingTier

  return rankingClass + " " + tier
}

function getMatchGameRoomStateChangedEventJson() {
  let logText
  try {
    logText = fs.readFileSync(outputFilePath, 'utf8')
  }
  catch (err) {
    return
  }

  try {
    let startText = "MatchGameRoomStateChangedEvent"
    let startIndex = logText.lastIndexOf(startText) + startText.length
    let trimmedLogText = logText.substr(startIndex)
    let endIndex = findClosingBracketMatchIndex(trimmedLogText, trimmedLogText.indexOf('{')) + 1

    // wrap return 
    return JSON.parse(trimmedLogText.substr(0, endIndex))
  }
  catch(err) {
    return false
  }
}

function getAuthenticateResponseJson() {
  let logText

  try {
    logText = fs.readFileSync(outputFilePath, 'utf8')
  }
  catch (err) {
    return
  }

  try {
    // Next level jank ahead :/
    let prodUri = fs.readFileSync(prodUriPath, 'utf8')
    let startIndex = logText.lastIndexOf( prodUri ) + prodUri.length

    if (startIndex == -1) {
      return undefined
    }

    let trimmedLogText = logText.substr(startIndex)
    let endIndex = findClosingBracketMatchIndex(trimmedLogText, trimmedLogText.indexOf('{')) + 1
    
    // Pretty much doing everything again witha different indexOf string
    let payloadString = "\"payloadObject\": "
    startIndex = trimmedLogText.indexOf(payloadString) + payloadString.length
    trimmedLogText = trimmedLogText.substr(startIndex)
    endIndex = findClosingBracketMatchIndex(trimmedLogText, trimmedLogText.indexOf('{'))

    // Have to add the } at the end becuase of jank
    return JSON.parse(trimmedLogText.substr(0, endIndex) + "}")
  }
  catch (err) {
    console.log("Couldn't get AuthenticateResponse JSON: " + err)
    return
  }
}

function getMatchCreatedJson() {
  let logText
  try {
    logText = fs.readFileSync(outputFilePath, 'utf8')
  }
  catch (err) {
    return
  }

  try {
    let matchString = "Incoming Event.MatchCreated "
    let startIndex = logText.lastIndexOf(matchString) + matchString.length
    let trimmedLogText = logText.substr(startIndex)
    let endIndex = trimmedLogText.indexOf('}') + 1

    return JSON.parse(trimmedLogText.substr(0, endIndex))
  }
  catch(err) {
    return
  }
} 

function findClosingBracketMatchIndex(str, pos) {
  let openBracketCount = 0;
  let givenBracketPosition = -1;
  let index = -1;
  for (let i = 0; i < str.length; i++) {
    let char = str[i];
    if (char === '{') {
      openBracketCount++;
      if (i === pos) {
        givenBracketPosition = openBracketCount;
      }
    } else if (char === '}') {
      if (openBracketCount === givenBracketPosition) {
        index = i;
        break;
      }
      openBracketCount--;
    }
  }
  return index;
}