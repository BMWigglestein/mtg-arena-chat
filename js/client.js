const io = require('socket.io-client')('http://162.243.170.228:3000');
// const io = require('socket.io-client')('http://localhost:3000');

window.onload = function () {

  let ipc = require('electron').ipcRenderer
  let sendButton = document.getElementById('send-button')
  let chatNodesWrap = document.getElementById('chat-nodes')

  window.addEventListener('resize', function() {
    toggleSubmissionWrap()
    toggleMuteButtonDisplay()
    toggleIndicatorBubbles()

    if (window.innerHeight == 83) { // if collapsing
      deleteAllChatNodes()
    }
    else { // if expanding you are in a match
      ipc.send('invokeAction', { message: "handsake" })
      ipc.once('actionReply', function (event, response) { // Message from Me
        io.emit('message.handshake', { 
          matchId: response.matchId,
          screenName: response.screenName,
          yourScreenName: response.yourScreenName,
        })
      })
    }
  })

  io.on('message.received', (message) => { // message from Opponent
    if (message.message == "handshake") {
      document.getElementById("bubble").src = "../assets/green_chat_bubble.png"
    }
    else {
      insertNewChatBubble(message.message, message.yourScreenName, false)
      chatNodesWrap.scrollTo(0, chatNodesWrap.scrollHeight)
    }
  })

  // window.addEventListener('resize', function(event) {
  // })
  document.getElementById('menu-dropdown').addEventListener('click', function(event) {
    toggleMenuDropdown()
  })
  document.getElementById('donate-btn').addEventListener('click', function(event) {
    openDonations()
    toggleMenuDropdown()
  })
  document.getElementById('mute-btn').addEventListener('click', function(event) {
    fadeChatNodes(1000)
    toggleMenuDropdown()
    toggleMuteButtonLabel()
  })
  document.getElementById('option-btn').addEventListener('click', function(event) {
    openOptionsWindow()
    toggleMenuDropdown()
  })

  sendButton.addEventListener('click', function (event) {
    event.preventDefault()

    let message = document.getElementById('send-message')

    ipc.send('invokeAction', { message: message.value })
    ipc.once('actionReply', function (event, response) { // Message from Me
      io.emit('message.send', { 
        matchId: response.matchId,
        screenName: response.screenName,
        yourScreenName: response.yourScreenName,
        eventId: response.eventId,
        opponentRank: response.opponentRank,
        message: response.message
      })
      insertNewChatBubble(response.message, response.yourScreenName, true)

      // If everything worked, clear the input
      message.value = ""
      // scroll to the bottom of the chat nodes
      chatNodesWrap.scrollTo(0, chatNodesWrap.scrollHeight)
    })
  })
}

function toggleMuteButtonDisplay() {
  let muteBtn = document.getElementById('mute-btn')
  let display = muteBtn.style.display

  muteBtn.style.display = (display == "none") ? "" : "none"
}
function toggleIndicatorBubbles() {
  let bubble = document.getElementById('indicator-bubble')
  let display = bubble.style.display

  if (display == "none") {
    document.getElementById("bubble").src = "../assets/red_chat_bubble.png"
  }

  bubble.style.display = (display == "none") ? "" : "none"
}
function toggleSubmissionWrap() {
  let wrap = document.getElementById('chat-submission-wrap')
  let display = wrap.style.display

  wrap.style.display = (display == "none") ? "" : "none"
}

function toggleMenuDropdown() {
  let dropdown = document.getElementById('dropdown')
  let display = dropdown.style.display

  dropdown.style.display = (display == "none") ? "" : "none"
}

function openDonations() {

}
function fadeChatNodes(speed) {
  let nodes = document.getElementById('chat-nodes')
  let targetOpacity = (nodes.style.opacity != "0") ? 0 : 1

  var seconds = speed/1000;

  nodes.style.transition = "opacity "+seconds+"s ease";
  nodes.style.opacity = targetOpacity;
}
function toggleMuteButtonLabel() {
  let label = (document.getElementById('chat-nodes').style.opacity != '0') ? "MUTE OPPONENT" : "UNMUTE OPPONENT"
  document.getElementById('mute-btn').innerHTML = label
}
function openOptionsWindow() {

}

function insertNewChatBubble(message, from, recieved) {
  // console.log(message, from, recieved)
  let sender = (recieved) ? "me" : "opponent"
  let hue = (recieved) ? "" : "darker"
  // todo: fix the oppoent and you screenName | will need to get your screenName a different way
  // let fromName = (recieved) ? "You" : `${from}`
  let fromName = (recieved) ? "You" : "Opponent"
  let time = getFormattedTime()

  chatNode = document.createElement('div')
  chatNode.className = `container ${hue} ${sender}`
  chatNode.innerHTML = `<p>${message}</p><span class='time-left'>${fromName} | ${time}</span>`
  document.getElementById('chat-nodes').appendChild(chatNode)
}

function deleteAllChatNodes() {
  let chatNodesWrap = document.getElementById('chat-nodes')
  chatNodesWrap.innerHTML = ""
}

function getFormattedTime() {
  let date = new Date()
  let hours = (date.getHours()).toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping:false})
  let mins = (date.getMinutes()).toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping:false})
  let secs = (date.getSeconds()).toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping:false})

  return `${hours}:${mins}:${secs}`
}
