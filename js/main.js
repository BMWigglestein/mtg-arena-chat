const electron = require('electron')
const app = electron.app
const BrowserWindow = electron.BrowserWindow
const logUtils = require('./log_utils.js')
let ipc = require('electron').ipcMain
const EventEmitter = require('events')
const emitter = new EventEmitter()
emitter.setMaxListeners(20)
const winWidth = 400
const collapseHeight = 83
const expandHeight = 600

// Disable error dialogs by overriding
const dialog = electron.dialog;
dialog.showErrorBox = function(title, content) {
    console.warn(`${title}\n${content}`);
};

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({width: winWidth, height: collapseHeight, resizable: false, transparent: true, frame: false})
  mainWindow.setFullScreenable(false);
  mainWindow.setMaximizable(false);

  // and load the index.html of the app.
  mainWindow.loadURL(`file://${__dirname}/../html/index.html`)

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

  // Needs to be on top of the game
  mainWindow.setAlwaysOnTop(true, "floating", 1)
  mainWindow.setVisibleOnAllWorkspaces(true)

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })
}

app.on('ready', function() {
  createWindow()

  logUtils.startOutputFileWatch()

  ipc.on('invokeAction', function(event, data) {
    event.sender.send('actionReply', {
      matchId: logUtils.getMatchId(),
      screenName: logUtils.getOpponent(),
      yourScreenName: logUtils.getYourScreenName(),
      eventId: logUtils.getEventId(),
      opponentRank: logUtils.getOpponentRank(),
      message: data.message
    })
  })

  // let i = 1
  setInterval( function() {
    console.log("your screenName: " + logUtils.getYourScreenName())
    if (logUtils.getInMatch()) {
      mainWindow.setContentSize(winWidth, expandHeight)
      // console.log(i + " You are currently in a match: " + logUtils.getMatchId())
    }
    else {
      mainWindow.setContentSize(winWidth, collapseHeight)
      // console.log(i + " Not in match")
    }
    // i++
  }, 1000)
})

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})
